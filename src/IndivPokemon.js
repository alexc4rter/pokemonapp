import React from "react";
import ReactDOM from "react-dom";

class IndivClass extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      index: 0,
      pokesdata: [],
      PokeImages: [],
      PokeAttributes: [],
      PokeMoves: [],
      PokeStats: [],
    };

    // This binding is necessary to make `this` work in the callback
    this.handleLeftClick = this.handleLeftClick.bind(this);
    this.handleRightClick = this.handleRightClick.bind(this);
  }

  async componentDidMount() {
    const data = await fetch("https://pokeapi.co/api/v2/pokemon/");
    const pokes = await data.json();
    this.setState({ pokesdata: pokes.results });
    console.log(pokes.results);

    const inddata = await fetch(pokes.results[this.state.index].url);
    const pokesd = await inddata.json();
    this.setState({ PokeImages: pokesd.sprites });
    this.setState({ PokeAttributes: pokesd.abilities });
    this.setState({ PokeMoves: pokesd.moves });
    this.setState({ PokeStats: pokesd.stats });
    console.log(pokesd.sprites.front_default);
    console.log(pokesd.abilities);
  }

  async NewLeftImage() {
    const inddata = await fetch(
      this.state.pokesdata[
        this.state.index > 0 ? this.state.index - 1 : this.state.index
      ].url
    );
    const pokesd = await inddata.json();
    this.setState({ PokeImages: pokesd.sprites });
    console.log(pokesd.sprites.front_default);
    this.setState({ PokeAttributes: pokesd.abilities });
    this.setState({ PokeStats: pokesd.stats });
    console.log(pokesd);
  }
  async NewRightImage() {
    const inddata = await fetch(
      this.state.pokesdata[
        this.state.index < this.state.pokesdata.length - 1
          ? this.state.index + 1
          : this.state.pokesdata.length - 1
      ].url
    );
    const pokesd = await inddata.json();
    this.setState({ PokeImages: pokesd.sprites });
    console.log(pokesd.sprites.front_default);
    this.setState({ PokeAttributes: pokesd.abilities });
    this.setState({ PokeStats: pokesd.stats });
    console.log("New Index state is " + this.state.index);
  }

  handleLeftClick() {
    console.log("Index state is " + this.state.index);
    //if
    this.state.index > 0
      ? this.setState((state) => ({
          index: state.index - 1,
        }))
      : //else
        this.setState((state) => ({
          index: 0,
        }));
    this.NewLeftImage();
  }
  handleRightClick() {
    console.log("Index state is " + this.state.index);
    //if
    this.state.index < this.state.pokesdata.length - 1
      ? this.setState((state) => ({
          index: state.index + 1,
        }))
      : //else
        this.setState((state) => ({
          index: state.index,
        }));
    this.NewRightImage();
  }

  render() {
    return (
      <div className="StatDiv">
        <button href="#" className="LeftButton" onClick={this.handleLeftClick}>
          &#8592;
        </button>
        <button
          href="#"
          className="RightButton"
          onClick={this.handleRightClick}
        >
          &#8594;
        </button>

        <div className="StatContentDiv">
          <div className="PokemonCardText">
            <h1 className="PokemonCardTextText">
              {this.state.pokesdata.length > 0
                ? this.state.pokesdata[this.state.index].name
                : ""}
            </h1>
          </div>
          <img
            className="PokemonComapareIMG"
            alt={`${this.state.index}`}
            src={this.state.PokeImages.front_default}
          ></img>
          <div className="StatsHolder">
            <div className="PokeStats">
              <h3 className="StatsTitle">Stats:</h3>
              <ul>
                {this.state.PokeStats.map((stats) => (
                  <li key={`${stats.stat.name}`}>
                    {stats.stat.name}: {stats.base_stat}
                  </li>
                ))}
              </ul>
            </div>

            <div className="PokeAttributes">
              <h3 className="StatsTitle">Attributes:</h3>
              <ul>
                {this.state.PokeAttributes.map((attri) => (
                  <li key={`${attri.ability.name}`}>{attri.ability.name}</li>
                ))}
              </ul>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default IndivClass;

ReactDOM.render(<IndivClass />, document.getElementById("root"));
