import React from "react";
import logo from "./pokeball.png";
import "./App.css";

function Nav() {
  return (
    <div className="Nav">
      <img src={logo} className="App-logo" alt="logo" />
      <div className="NavHolder">
        <div className="TitleDiv">
          <h1 className="Title">Poke-Nav</h1>
        </div>
        <div className="SearchDiv">
          <h1 className="SearchPlaceholder"></h1>
        </div>
      </div>
    </div>
  );
}

export default Nav;
