import React from "react";
import Nav from "./Nav";
import Pokemon from "./Pokemon";
import "./App.css";
import InvidPokemon from "./IndivPokemon";
import Testclass from "./testclass";

function App() {
  return (
    <div className="App">
      <Nav></Nav>
      <div className="Spacer"></div>
      <div className="Body">
        <InvidPokemon></InvidPokemon>
        <InvidPokemon></InvidPokemon>
        <Pokemon></Pokemon>
      </div>
    </div>
  );
}

export default App;
