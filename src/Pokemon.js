import React, { useState, useEffect } from "react";
import IndivPokemon from "./IndivPokemon";
import "./App.css";

function Pokemon() {
  useEffect(() => {
    fetchItems();
  }, []);

  const [pokes, setPokes] = useState([]);
  const [ImgUrl, setImgUrl] = useState([]);
  const PokeAPI = new Array();
  const ImgUrls = new Array();
  var num = 0;

  const fetchItems = async () => {
    const data = await fetch("https://pokeapi.co/api/v2/pokemon/");

    const pokes = await data.json();
    //console.log(pokes);
    setPokes(pokes.results);

    pokes.results.forEach((element) => {
      PokeAPI.push(element.url);

      //log the individual Pokemon API urls
      console.log(element.url);

      num++;
    });

    const tmp = await fetchImages();

    //setImgUrl(ImgUrls);

    //console.log(ImgUrl);
    console.log(ImgUrls);
  };

  const fetchImages = async () => {
    for (let index = 0; index < 20; index++) {
      const data = await fetch(PokeAPI[index]);
      const tmpImgUrl = await data.json();
      ImgUrls.push(tmpImgUrl.sprites.front_default);
      //return sleep(1000).then(console.log(ImgUrls[index].sprites));
    }
  };

  //attemp to fix the return before api fetch recieived
  const sleep = (ms) => {
    return new Promise((resolve) => setTimeout(resolve, ms));
  };

  return (
    <div className="PokemonList">
      {pokes.map((name, index) => (
        <div className="PokemonCard" key={name.name}>
          <a
            href={`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${
              index + 1
            }.png`}
          >
            <img
              className="PokemonIMG"
              alt={`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${
                index + 1
              }.png`}
              src={`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${
                index + 1
              }.png`}
              key={index}
            ></img>
            <h2 className="PokemonCardText">{name.name}</h2>
          </a>
        </div>
      ))}
    </div>
  );
}

export default Pokemon;
